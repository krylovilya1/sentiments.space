README
=====================

##### Команды

* `docker-compose run --rm --service-ports server` - запустить локально веб приложение
* `docker exec -i $(docker ps | grep server_ | awk '{{ print $1 }}') python manage.py migrate *apps*` - применить миграции
* `docker exec -i $(docker ps | grep server_ | awk '{{ print $1 }}') python manage.py makemigrations *apps*` - создать файл миграций
* `docker exec -it $(docker ps | grep server_ | awk '{{ print $1 }}') python manage.py createsuperuser` - создать супер пользователя
* `docker exec -it $(docker ps | grep server_ | awk '{{ print $1 }}') python manage.py shell` - зайти в shell django приложения
* `docker exec -it $(docker ps | grep server_ | awk '{{ print $1 }}') bash` - зайти в bash контейнера server
* `docker kill $(docker ps -q)` - остановить все запущенные контейнеры


##### Команды для первого запуска

* `docker-compose build` - создать контейнеры docker
* `docker-compose run --rm --service-ports server` - запустить веб приложение
* `docker exec -i $(docker ps | grep server_ | awk '{{ print $1 }}') python manage.py migrate` - применить миграции

##### Команды для последующего запуска

* `docker-compose run --rm --service-ports server` - запустить веб приложение
* `docker exec -i $(docker ps | grep server_ | awk '{{ print $1 }}') python manage.py migrate` - применить миграции
