from django.urls import include, path
from .views import AccountView, VkLinkView, SentimentView


urlpatterns = [
    path('', AccountView.as_view()),
    path('vk_link/', VkLinkView.as_view()),
    path('results/', SentimentView.as_view())

]
