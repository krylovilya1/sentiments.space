import math
import re

import requests
from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import View
from dostoevsky.models import FastTextSocialNetworkModel
from dostoevsky.tokenization import RegexTokenizer

from .models import UserModel


class AccountView(LoginRequiredMixin, View):
    def get(self, request):
        user = request.user
        need_vk_link = UserModel.objects.filter(user=user).count() == 0
        return render(request, 'account/account.html', {'need_vk_link': need_vk_link})


class VkLinkView(LoginRequiredMixin, View):
    def get(self, request):
        user = request.user
        if UserModel.objects.filter(user=user).count():
            return HttpResponse(status=500)
        client_id = settings.VK_client_id
        redirect_uri = 'https://sentiments.space/account/vk_link/'
        state = request.GET.get('state', None)

        if state == 'code':
            code = request.GET.get('code', None)
            if not code:
                return HttpResponse(status=500)
            client_secret = settings.VK_client_secret
            response = requests.get(f"https://oauth.vk.com/access_token?client_id={client_id}"
                                    f"&redirect_uri={redirect_uri}&client_secret={client_secret}&code={code}").json()
            user_vk_id = int(response['user_id'])
            access_token = response['access_token']
            obj = UserModel.objects.create(user=user, user_vk_id=user_vk_id, access_token=access_token)
            if obj:
                return redirect('/account/')
            return HttpResponse(status=500)

        response_type = 'code'
        state = 'code'
        scope = 'groups,wall,offline'
        return redirect(f"https://oauth.vk.com/"
                        f"authorize?client_id={client_id}&redirect_uri={redirect_uri}"
                        f"&response_type={response_type}&scope={scope}&state={state}")


class VkGetComments(LoginRequiredMixin, View):
    pass


class CommentsResearcher:
    """ Класс для анализа тональности комментариев """

    def __init__(self):
        self.tokenizer = RegexTokenizer()
        self.model = FastTextSocialNetworkModel(tokenizer=self.tokenizer)

    def get_sentiment(self, commens_list):
        """ Опредение тональности комментариев
        Возвращает 2 значения:
        - distribution (dict)
            словарь в количеством комментариев в разных категориях
        - sentiments (dict)
            словарь с комментариями в каждой категории
        """

        comments = commens_list

        # получение тональности для комментариев с помощью модели
        results = self.model.predict(comments[0:-1], k=1)
        distribution = {}
        output = []

        sentiments = {
            'positive': [],
            'negative': [],
            'neutral': [],
            'skip': [],
            'speech': []
        }

        for comment, sentiment in zip(comments, results):
            comment_sentiment = list(sentiment.keys())[0]
            output.append(comment_sentiment)
            sentiments[comment_sentiment].append(comment)

        # подсчет результатов для каждой категории
        distribution['positive'] = output.count('positive')
        distribution['negative'] = output.count('negative')
        distribution['neutral'] = output.count('neutral')
        distribution['skip'] = output.count('skip')
        distribution['speech'] = output.count('speech')

        return distribution, sentiments


class SentimentView(LoginRequiredMixin, View):
    def get(self, request):
        url = 'https://api.vk.com/method/wall.getById'
        user = request.user
        access_token = UserModel.objects.get(user=user).access_token
        posts = request.GET.get('url_field', '')
        if posts == '':
            return HttpResponse('400 Bad Request', status=400)
        posts = re.search(r'wall(-\d+_\d+)', posts)[1]
        params = {
            'access_token': access_token,
            'v': '5.126',
            'posts': posts,
        }
        response = requests.get(url, params=params).json()
        comment_number = int(response['response'][0]['comments']['count'])

        url = 'https://api.vk.com/method/wall.getComments'
        owner_id, post_id = posts.split("_")
        params = {
            'access_token': access_token,
            'v': '5.126',
            'owner_id': owner_id,
            'post_id': post_id,
            'need_likes': '0',
            'count': '100',
            'thread_items_count': '10',
        }
        iterations = math.ceil(comment_number / 100)
        offset = 0
        comments = []
        for i in range(iterations):
            current_params = params.copy()
            current_params.update({'offset': offset})
            response = requests.get(url, params=current_params).json()
            for current_comment in response['response']['items']:
                comments.append(current_comment['text'])
                for current_thread_comment in current_comment['thread']['items']:
                    comments.append(current_thread_comment['text'])
            offset += 100
        final_comments = []
        for el in comments:
            tmp = re.sub(r"\[id\w+],", '', el.replace("|", ""))
            if tmp:
                final_comments.append(tmp)

        researcher = CommentsResearcher()
        result = researcher.get_sentiment(final_comments)
        distribution = result[0]
        print(result[1])

        args = dict()
        args['negative'] = distribution['negative']
        args['positive'] = distribution['positive']
        args['neutral'] = distribution['neutral']
        args['positive_comments'] = result[1]['positive']
        args['negative_comments'] = result[1]['negative']

        return render(request, 'account/results.html', args)
