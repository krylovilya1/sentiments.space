from django.db import models
from django.contrib.auth.models import User


class UserModel(models.Model):
    user = models.ForeignKey(User, verbose_name="Ид пользователя", on_delete=models.CASCADE)
    user_vk_id = models.IntegerField(verbose_name='Ид пользователя в вк')
    access_token = models.TextField(verbose_name="Ключ пользователя", max_length=255)
