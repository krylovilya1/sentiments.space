#!/bin/bash

docker stop $(docker ps -aq)
systemctl restart docker
docker-compose -f docker-compose.prod.yml build
docker-compose -f docker-compose.prod.yml up -d
docker_id=$(docker ps | grep _server | awk '{{ print $1 }}')
docker exec -i $docker_id python manage.py makemigrations
docker exec -i $docker_id python manage.py migrate
